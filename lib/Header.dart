import 'package:flutter/material.dart';
import 'Ejemplo1.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Header extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Service service = new Service(context);

    final button = new InkWell(
      child: new Container(
        margin: new EdgeInsets.only(
          top: 30.0,
          left: 20.0,
          right: 20.0,
        ),
        height: 50.0,
        width: 180.0,
        decoration: new BoxDecoration(
          boxShadow: [
            new BoxShadow(
              color:  Color(0xff6a1b9a),
              offset: new Offset(10.0, 10.0),
              blurRadius: 30.0
            )
          ],
          borderRadius: new BorderRadius.circular(30.0),
          color: Color(0xFF6a1b9a)
        ),
        child: new RaisedButton(
            onPressed: service.getData,
          child: new Center(
            child: new Text(
              "obtener datos",
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                  fontWeight: FontWeight.w900
              ),
            ),
          ),

        ),

      )
    );

    var texto =   new Container(
      alignment : Alignment.center,
      margin: new EdgeInsets.only(
        top:  100.0
      ),
      child: new Column(
          children: <Widget>[
            new Text(
              service.output,
              style: const TextStyle(
                  fontSize: 30.0,
                  color: Color(0xff6a1b9a),
                  fontWeight: FontWeight.w300
              ),
            )
          ],
      )
    );

    return new Scaffold(
      body:  new Stack(
        children: <Widget>[
          new Ejemplo1(),
          new Container(
            alignment: Alignment.center,
            margin: new EdgeInsets.only(
              top: 50.0
            ),
            child: new Column(
              children: <Widget>[
                new Text(
                  "Hola mundo",
                  style: const TextStyle(
                    fontSize: 55.0,
                    color: Color(0xff6a1b9a),
                    fontWeight: FontWeight.w600
                  ),
                ),
                texto,
                button
              ],
            ),
          )
        ],
      ),
    );
  }
}


class Service{
  BuildContext context;

  Service(BuildContext context){
    this.context = context;
  }
  String output = "...";

  Future<String> getData() async{

    http.Response response = await http.get(
        Uri.encodeFull("https://systran-systran-platform-for-language-processing-v1.p.rapidapi.com/translation/text/translate?source=auto&target=es&input=dog"),
        headers: {
          "X-RapidAPI-Host": "systran-systran-platform-for-language-processing-v1.p.rapidapi.com",
          "X-RapidAPI-Key": "6f8c0ed8d8msh863fc4f6587fca3p15487djsne19c87458061"
        }
    );

    var data = json.decode(response.body);
    output = response.body;
    alert(context);

  }

  void alert(BuildContext context){
    var alertDialog = AlertDialog(
      title: Text("respuesta"),
      content: Text(output),
    );
    showDialog(
        context: context,
        builder: (BuildContext context){
          return alertDialog;
      }
    );
  }

}


